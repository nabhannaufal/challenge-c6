const express = require('express');
const app = express();
const session = require('express-session')
const { user_admin, user_game, user_game_biodata, user_game_history } = require('./models');

app.use(express.json());

app.use(session({
  secret: 'nabhan',
  name: 'uniqueSessionID',
  saveUninitialized: false
}))

app.use(express.urlencoded({
  extended: false
}));

app.use(express.static('public'))

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  res.redirect('/login')
})

app.get('/login', (req, res) => {
  res.render('login_view')
})

app.get('/logout', (req, res) => {
  req.session.loggedIn = false;
  res.render('login_view')
})

app.post('/login', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  user_admin.findAll({
    where: {
      username: username,
      password: password
    }
  }).then(userGame => {
    if (userGame.length === 0) {
      return res.redirect(301, '/login')
    }
    req.session.loggedIn = true;
    res.redirect('/dashboard');
  })
})

app.get('/dashboard', (req, res) => {
  if (req.session.loggedIn) {
    user_game.findAll().then(userGame =>
      user_game_biodata.findAll().then(userBio =>
        user_game_history.findAll().then(userHistory =>
          user_admin.findAll().then(userAdmin =>
            res.render('dashboard_view', { userGame, userBio, userHistory, userAdmin }))))
    )
  } else {
    res.redirect('/login')
  }
})

app.get('/history', (req, res) => {
  if (req.session.loggedIn){
    user_game_history.findAll().then(userHistory =>
      res.render('history_view', { userHistory})
  );
  }else{
    res.redirect('/login')
  }
})

app.get('/user', (req, res) => {
  if (req.session.loggedIn){
    user_game.findAll().then(userGame =>
      user_admin.findAll().then(userAdmin =>
        res.render('user_view', { userGame, userAdmin }))
  );
  }else{
    res.redirect('/login')
  }
})


app.get('/user/add', (req, res) => {
  res.render('addUserView');
})

app.post('/user/save', (req, res) => {
  user_game.create({
    username: req.body.username,
    password: req.body.password,
    cash: req.body.cash,
    level: req.body.level
  }).then(userGame => {
    user_game_biodata.create({
      namaLengkap: req.body.namaLengkap,
      email: req.body.email,
      alamat: req.body.alamat,
      noTelpon: req.body.noTelpon,
      urlFoto: req.body.urlFoto
    }).then(biodata => res.redirect(301, '/dashboard'))
  })
})

app.get('/admin/add', (req, res) => {
  res.render('addAdminView');
})

app.post('/admin/save', (req, res) => {
  user_admin.create({
    username: req.body.username,
    password: req.body.password
  }).then(biodata => res.redirect(301, '/user'))
})



app.get('/user/delete/:id', (req, res) => {
  const userId = req.params.id;

  user_game_biodata.destroy({
    where: {
       id: userId
    }
  }).then(biodata => {
    user_game.destroy({
      where: {
        id: userId
      }
    }).then(user => {
      res.redirect(301, '/dashboard');
    })
  })
})

app.get('/admin/delete/:id', (req, res) => {
  const adminId = req.params.id;
  user_admin.destroy({
    where: {
      id: adminId
   }
  }).then(admin => res.redirect(301, '/user'))
})


app.get('/user/update/:id', (req, res) => {
  const userId = req.params.id;

  user_game.findOne({
    where: {
      id: userId
    }
  }).then(user => {
    user_game_biodata.findOne({
      where: {
        id: user.id
      }
    }).then(biodata => {
      res.render('updateUserView', { user, biodata })
    })
  })
})

app.post('/user/update/:id', (req, res) => {
  const userId = req.params.id;

  user_game.update({
    username: req.body.username,
    password: req.body.password,
    cash: req.body.cash,
    level: req.body.level
  }, {
    where: {
      id: userId
    }
  }).then(user => {
    user_game_biodata.update({
      namaLengkap: req.body.namaLengkap,
      email: req.body.email,
      alamat: req.body.alamat,
      noTelpon: req.body.noTelpon,
      urlFoto: req.body.urlFoto
    }, {
      where: {
        id: userId
      }
    }).then(biodata => {
      res.redirect(301, '/dashboard')
    })
  })
})

app.get('/admin/update/:id', (req, res) => {
  const adminId = req.params.id;
  user_admin.findOne({
    where: {
      id: adminId
   }
  }).then(admin => res.render('updateAdminView', { admin }))
})

app.post('/admin/update/:id', (req, res) => {
  const adminId = req.params.id;
  user_admin.update({
    username: req.body.username,
    password: req.body.password
  }, {
    where: {
      id: adminId
    }
  }).then(admin => res.redirect(301, '/user'))
})

app.get('/api/user', (req, res) => {
  user_game.findAll().then(userGame => {
    res.status(200).json(userGame)
  })
});

app.post('/api/user/update/:id', (req, res) => {
  const userId = req.params.id;

  user_game.update({
    username: req.body.username,
    password: req.body.password,
    cash: req.body.cash,
    level: req.body.level
  }, {
    where: {
      id: userId
    }
  }).then(user => {
    user_game_biodata.update({
      namaLengkap: req.body.namaLengkap,
      email: req.body.email,
      alamat: req.body.alamat,
      noTelpon: req.body.noTelpon,
      urlFoto: req.body.urlFoto
    }, {
      where: {
        id: userId
      }
    }).then(biodata => {
      res.status(200).json({ message: "berhasil update data" })
    })
  })
})

app.listen(3000, () => console.log('apps sudah jalan di port 3000'));