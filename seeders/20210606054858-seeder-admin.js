'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_admins', [{
      username: 'admin',
      password: 'admin',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      username: 'nabhan',
      password: 'nabhan',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_admins', null, {});
  }
};
